# rush00_correction

## Getting started

Just download this repository inside the repository you want to correct.

The repository must be organizd like this:
```
./
## This is what you want to correct ##
|-- ex00/
|   |-- rushXX.c
|   |-- main.c
|   |-- ft_putchar.c

## This is the tester ##
|-- MatTester/
|   |-- test.sh
|   |-- ex00/
|       |-- rush00.c
|       |-- rush01.c
|       |-- rush02.c
|       |-- rush03.c
|       |-- rush04.c
|       |-- main.c
|       |-- ft_putchar.c
```

You may encounter errors if 42 norminette is not install and if there are non-desired files inside the repo (PDF subject for example).

***
## Rush00 source

Link to my rush:
https://gitlab.com/annexes/rush00
